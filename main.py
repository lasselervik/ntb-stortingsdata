
from flask import Flask, send_file, render_template, request, make_response
from flask_basicauth import BasicAuth
from ftplib import FTP
import time
import os

app = Flask(__name__)

app.config['BASIC_AUTH_USERNAME'] = 'ntb'
app.config['BASIC_AUTH_PASSWORD'] = 'test'
app.config['BASIC_AUTH_FORCE'] = True

basic_auth = BasicAuth(app)

@app.route("/")
@basic_auth.required
def index():
    return render_template("index.html")

# Skrur på debug-modus i Flask, som lar deg endre kode uten restart

if __name__ == "__main__":
    app.run(debug=False)