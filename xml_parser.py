import xml.etree.ElementTree as ET
import requests

response_representant = requests.get('https://data.stortinget.no/eksport/representanter?stortingsperiodeid=2017-2021')
response_votering = requests.get('https://data.stortinget.no/eksport/voteringsresultat?VoteringId=1499')
root = ET.fromstring(response_representant.content)
root2 = ET.fromstring(response_votering.content)
ns = {'xmlns': 'http://data.stortinget.no'}

date_today = time.strftime("%d-%m-%Y")
json_filename = 'Votering_' + date_today + "_" + ".json"

#response_representant variables

politician_id = [child.text for child in root.findall('xmlns:representanter_liste/xmlns:representant/xmlns:id', ns)]
fornavn = [child.text for child in root.findall('xmlns:representanter_liste/xmlns:representant/xmlns:fornavn', ns)]
etternavn = [child.text for child in root.findall('xmlns:representanter_liste/xmlns:representant/xmlns:etternavn', ns)]
fullt_navn = [" ".join(navn) for navn in list(zip(fornavn, etternavn))]
foedselsdato = [child.text for child in root.findall('xmlns:representanter_liste/xmlns:representant/xmlns:foedselsdato', ns)]
navn_fylke = [child.text for child in root.findall('xmlns:representanter_liste/xmlns:representant/xmlns:fylke/xmlns:navn', ns)]
navn_parti = [child.text for child in root.findall('xmlns:representanter_liste/xmlns:representant/xmlns:parti/xmlns:navn', ns)]

#response_votering variables

given_vote = [child.text for child in root2.findall('xmlns:voteringsresultat_liste/xmlns:representant_voteringsresultat/xmlns:votering', ns)]
vote_id = [child.text for child in root2.findall('xmlns:votering_id/', ns)]
politician_id2 = [child.text for child in root2.findall('xmlns:voteringsresultat_liste/xmlns:representant_voteringsresultat/xmlns:representant/xmlns:id', ns)]

for i, navn in enumerate(fullt_navn):
    print(f'{fullt_navn[i]} ({politician_id[i]}) er født i {foedselsdato[i]} og representerer {navn_parti[i]} i {navn_fylke[i]}')

votations = []
for i, navn in enumerate(given_vote):
    print(f'{politician_id2[i]} stemte {given_vote[i]} ')
    votations.append(f'{politician_id2[i]} stemte {given_vote[i]}')